import java.util.Arrays;

public class ListaSpesa {
    private Prodotti spesa[];
    private int numProd=0;
    private double totale;
    private boolean cartaFidaty= false;

    public ListaSpesa(int maxSpesa) {
        this.spesa = new Prodotti[maxSpesa];
    }

    public void carta(String siOno){
        if(siOno == "si"){
            cartaFidaty = true;
        }
        else{
            cartaFidaty = false;
        }
    }

    public void aggiungiProdotto(Prodotti prod){//AGGIUNGE IL PRODOTTO E SCONTA IL PREZZO.
        int tipo =0;
        spesa[numProd] = prod;
        if(prod instanceof Alimentari){
            tipo=1;
        }else if(prod instanceof NonAlimentari){
            tipo=2;
        }else{
            tipo=3;
        }

        switch(tipo) {

                case 1:

                    Alimentari alimento = (Alimentari)spesa[numProd]; /* PER FARE IL CAST DEVO PASSARE SEMPRE DA UN'ALTRA VARIABILE
                    MA CIÒ MEMORIZZERA COMUNQUE NELLA POSIZIONE DEL VETTORE SPECIFICATA IL TIPO CHE HO SCELTO IO!!! */
                    if(cartaFidaty) {
                        alimento.applicaSconto(alimento.getDataScadenza());
                    }else{
                        alimento.applicaSconto();
                    }
                    numProd++;
                    break;
                case 2:

                    NonAlimentari nonAlimento = (NonAlimentari)spesa[numProd];
                    if(cartaFidaty) {
                        nonAlimento.applicaSconto2();
                    }else{
                        nonAlimento.applicaSconto();
                    }
                    numProd++;
                    break;
                case 3:default:

                    spesa[numProd].applicaSconto();
                    numProd++;
                    break;
            }
    }

    public double totaleSpesa(){
        totale=0;
        for(int i=0;i<spesa.length;i++){
            totale=totale + spesa[i].prezzo;
        }
        return totale;
    }

    @Override
    public String toString() {
        return "ListaSpesa{" +
                "spesa=" + Arrays.toString(spesa) +
                '}';
    }

    public void ordinaProdotti(){
        Arrays.sort(spesa,0,numProd);
    }

    public void eliminaProdotto(Prodotti prod){
        for(int i=0;i<numProd;i++) {
            if (spesa[i].equals(prod)) {
                spesa[i] = null;
                numProd--;
            }
        }
    }

}
