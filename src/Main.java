public class Main {
    public static void main(String[] args) {
        Data dataScadenza = new Data(10,10,2020);
        Data dataAttuale = new Data(1,10,2020);
        Prodotti prod = new Prodotti("0",25);
        Alimentari alim = new Alimentari("1",50,dataScadenza);
        NonAlimentari nonAlim = new NonAlimentari("2",5,"CARTA");

        ListaSpesa lista = new ListaSpesa(5);

        lista.carta("si");
        lista.aggiungiProdotto(prod);
        lista.aggiungiProdotto(alim);
        lista.aggiungiProdotto(nonAlim);

        System.out.println(lista.toString());
        lista.ordinaProdotti();
        System.out.println(lista.toString());

        lista.eliminaProdotto(alim);
        System.out.println(lista.toString());


    }
}
