public class NonAlimentari extends Prodotti {
    private String materiale;
    private final double SCONTO= (double)10/100;

    public NonAlimentari(String codice, double prezzo, String materiale) {
        super(codice, prezzo);
        this.materiale = materiale;
    }

    public double applicaSconto2(){
        if(materiale.equals(Materiale.CARTA) || materiale.equals(Materiale.PLASTICA) || materiale.equals(Materiale.VETRO)){
            prezzo = prezzo - SCONTO*prezzo;
            return prezzo;
        }
        else{
            return super.applicaSconto();
        }
    }

    @Override
    public String toString() {
        return "NonAlimentari{" +
                "materiale='" + materiale + '\'' +
                ", SCONTO=" + SCONTO +
                ", prezzo=" + prezzo +
                '}';
    }
}
