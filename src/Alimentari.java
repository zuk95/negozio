
public class Alimentari extends Prodotti {
    private Data dataScadenza;
    private final double SCONTO= (double)20/100;

    public Alimentari(String codice, double prezzo, Data dataScadenza) {
        super(codice, prezzo);
        this.dataScadenza = dataScadenza;
    }

    public double applicaSconto(Data dataAttuale){
        if((dataAttuale.getDifference(dataScadenza)) < 10){
            prezzo = prezzo - SCONTO*prezzo;
            return prezzo;
        }
        else{
            return super.applicaSconto();
        }

    }

    public Data getDataScadenza() {
        return dataScadenza;
    }

    @Override
    public String toString() {
        return "Alimentari{" +
                "dataScadenza=" + dataScadenza +
                ", SCONTO=" + SCONTO +
                ", prezzo=" + prezzo +
                '}';
    }
}
