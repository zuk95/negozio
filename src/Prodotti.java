public class Prodotti implements Comparable {
    private String codice;
    private String descrizione;
    protected double prezzo;
    private final double SCONTO = (double)5/100;

    public Prodotti(String codice, double prezzo) {
        this.codice = codice;
        if(prezzo>=0){this.prezzo = prezzo;}else{prezzo=0;};
    }

    public String getCodice() {
        return codice;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public double applicaSconto(){
        prezzo = prezzo - (SCONTO)*prezzo;

        return prezzo;
    }

    @Override
    public String toString() {
        return "Prodotti{" +
                "codice='" + codice + '\'' +
                ", prezzo=" + prezzo +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Prodotti a = (Prodotti)o;
        return (int)(this.getPrezzo() - a.getPrezzo());
    }
}
